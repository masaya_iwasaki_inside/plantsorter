import argparse
import json
from utils import json_glob


def main():
    # Parsing arguments
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('image_dir', type=str)

    args = parser.parse_args()

    json_path_list = json_glob(args.image_dir)
    print(len(json_path_list))

    with open(json_path_list[0], 'r') as f:
        json_data = json.load(f)
    print(json.dumps(json_data, ensure_ascii=True, indent=1))


if __name__ == '__main__':
    main()
