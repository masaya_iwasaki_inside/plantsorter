# plant data整理用リポジトリ

## 1. 実行

```bash
python sorter.py {image dir} {read data type}
# example
# python sorter.py test_images all --move_type copy
```

## 2. アルゴリズム
###2.1 空欄判別モデルでwrittenの画像を抽出
model: 20180115リリースの空欄判別モデル  
目的: 空欄画像やクロップずれ画像を除外  
###2.2 detectionモデルで1Lineの画像を抽出  
   model: 20180507リリースのdetectionモデル  
   目的: 複数行を除外  
###2.3 2つのモデルでOCR結果が異なるもの画像を抽出  
   model1: 20180709リリースのallモデル  
   model2: 20180417リリースのallモデル  
   目的: 学習に有効な画像のみを抽出  
