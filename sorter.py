import os
import gc
import re
import cv2
import argparse
import pytz
import shutil
import json
from datetime import datetime
from keras import backend as K

from utils import img_glob
from sorter import BlankRecognizer
from sorter import DetectionSorter
from sorter import AllRecognizer
from utils import aws_handler
from utils import image_preprocessing


class Sorter(object):
    s3_all_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_all_20180709.aiinside'
    s3_old_all_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_all_20180417.aiinside'
    s3_blank_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_blank_20180115.aiinside'
    s3_detection_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_detection_20180730.aiinside'

    def __init__(self, work_space_path, batch_size, read_model_type):
        self.models_path = self.__download_model_files(work_space_path)
        self.read_model_type = read_model_type
        self.batch_size = batch_size
        self._load_models()

        self.date = datetime.strftime(datetime.now(pytz.timezone('Japan')), '%m%d%H%M')
        self.save_count = 1
        self.rep = '^[A-Za-z0-9@\*\+\-\.\(\)\$\[\]_:;<>=!?#%&×÷]+$'

    def _load_models(self):
        self.blank_recognizer = BlankRecognizer(self.models_path['blank'], batch_size=self.batch_size)
        self.detection_recognizer = DetectionSorter(self.models_path['detection'])
        self.ocr_recognizer = AllRecognizer(self.models_path['all'], batch_size=self.batch_size)
        self.old_ocr_recognizer = AllRecognizer(self.models_path['old_all'], batch_size=self.batch_size, scaling=False)

    def _destroy(self):
        if self.blank_recognizer is not None:
            del self.blank_recognizer
        if self.detection_recognizer is not None:
            del self.detection_recognizer
        if self.ocr_recognizer is not None:
            del self.ocr_recognizer
        if self.old_ocr_recognizer is not None:
            del self.old_ocr_recognizer
        K.clear_session()
        gc.collect()

    def refresh_model(self):
        self._destroy()
        self._load_models()

    @staticmethod
    def __download_model_files(work_space_path):
        work_space_path = os.path.join(work_space_path, 'Model')
        os.makedirs(work_space_path, exist_ok=True)

        models_path = {'all': aws_handler.download_file(Sorter.s3_all_path, work_space_path),
                       'old_all': aws_handler.download_file(Sorter.s3_old_all_path, work_space_path),
                       'blank': aws_handler.download_file(Sorter.s3_blank_path, work_space_path),
                       'detection': aws_handler.download_file(Sorter.s3_detection_path, work_space_path),
                       }

        return models_path

    def rename_and_move(self, image_path_list, move_dir):
        for i in range(len(image_path_list)):
            img_path = image_path_list[i]
            if isinstance(img_path, dict):
                text = img_path['text'].replace('_', '＿').replace(':', '：').replace('/', '／')
                if len(text) > 35:
                    text = text[:35]
                img_path = img_path['filename']
                image_name = '{}_{}_{:07d}_{}.png'.format(self.date, self.read_model_type, self.save_count, text)
                image_name = os.path.join(move_dir, image_name)
            else:
                image_name = '{}_{}_{:07d}.png'.format(self.date, self.read_model_type, self.save_count)
                image_name = os.path.join(move_dir, image_name)

            shutil.move(img_path, image_name)
            if os.path.isfile(img_path.replace('.png', '.json')):
                shutil.move(img_path.replace('.png', '.json'), image_name.replace('.png', '.json'))
            self.save_count += 1

    def rename_and_copy(self, image_path_list, move_dir):
        for i in range(len(image_path_list)):
            img_path = image_path_list[i]
            if isinstance(img_path, dict):
                text = img_path['text'].replace('_', '＿').replace(':', '：').replace('/', '／')
                if len(text) > 35:
                    text = text[:35]
                img_path = img_path['filename']
                image_name = '{}_{}_{:07d}_{}.png'.format(self.date, self.read_model_type, self.save_count, text)
                image_name = os.path.join(move_dir, image_name)
            else:
                image_name = '{}_{}_{:07d}.png'.format(self.date, self.read_model_type, self.save_count)
                image_name = os.path.join(move_dir, image_name)

            shutil.copy(img_path, image_name)
            if os.path.isfile(img_path.replace('.png', '.json')):
                shutil.copy(img_path.replace('.png', '.json'), image_name.replace('.png', '.json'))
            self.save_count += 1

    @staticmethod
    def _prepare_output_dir(root_dir):
        output_dir = os.path.join(root_dir, 'output')
        os.makedirs(output_dir, exist_ok=True)
        remove_dir = {'empty': os.path.join(output_dir, 'empty'),
                      'detection': os.path.join(output_dir, 'multi_line'),
                      'ocr': os.path.join(output_dir, 'ocr'),
                      }

        dataset_root = os.path.join(output_dir, 'dataset')
        os.makedirs(dataset_root, exist_ok=True)
        dataset_dir = {'1': os.path.join(dataset_root, '1'),
                       '2': os.path.join(dataset_root, '2'),
                       '3': os.path.join(dataset_root, '3'),
                       '0.5': os.path.join(dataset_root, '0.5'),
                       }
        for sort_type in remove_dir:
            os.makedirs(remove_dir[sort_type], exist_ok=True)

        for output_type in dataset_dir:
            os.makedirs(dataset_dir[output_type], exist_ok=True)
        return remove_dir, dataset_dir

    def recognizer_tester(self, image_dir, test_num=10):
        image_path_list = sorted(img_glob(image_dir))
        image_path_list = image_path_list[:test_num]
        imgs = [cv2.imread(image_path, cv2.IMREAD_COLOR) for image_path in image_path_list]

        blank_results = self.blank_recognizer.predict(imgs)
        detect_results, _ = self.detection_recognizer.predict(imgs)
        ocr_results = self.ocr_recognizer.predict(imgs)
        old_ocr_results = self.old_ocr_recognizer.predict(imgs)

        for i in range(len(image_path_list)):
            print(image_path_list[i])
            print('blank result :', blank_results[i])
            print('detect num :', len(detect_results[i]) if detect_results[i][0] is not None else None)
            print('ocr result :', ocr_results[i])
            print('ocr result(old model) :', old_ocr_results[i])

            print()

    @staticmethod
    def detect_tester(image_dir, output_dir, work_space_path):
        os.makedirs(output_dir, exist_ok=True)
        # prepare model
        models_path = Sorter.__download_model_files(work_space_path)
        detection_recognizer = DetectionSorter(models_path['detection'])

        # prepare image
        image_path_list = sorted(img_glob(image_dir))
        image_path_list = image_path_list[:10]
        imgs = [image_preprocessing(cv2.imread(p, cv2.IMREAD_COLOR)) for p in image_path_list]

        # detect
        _, boxes = detection_recognizer.predict(imgs)

        # draw rectangle
        for i in range(len(boxes)):
            img = imgs[i]
            if boxes[i] is not None:
                for box in boxes[i]:
                    cv2.rectangle(img, (box[2], box[0]), (box[3], box[1]), (0, 0, 255), 1)

        for path, img in zip(image_path_list, imgs):
            img_name = os.path.join(output_dir, os.path.basename(path))
            cv2.imwrite(img_name, img)

    def sorting_flow(self, image_dir, move_type):
        def blank_judge(img_path_list, imgs, results):
            my_img_path_list, my_retire_path_list, my_imgs = [], [], []
            for img_path, img, result in zip(img_path_list, imgs, results):
                if result == 'written':
                    my_img_path_list.append(img_path)
                    my_imgs.append(img)
                else:
                    my_retire_path_list.append(img_path)
            return my_img_path_list, my_retire_path_list, my_imgs

        def detection_judge(img_path_list, imgs, results):
            my_img_path_list, my_imgs = [], []
            my_retire_path_list = {'blank': [], 'multi': []}
            for img_path, img, result in zip(img_path_list, imgs, results):
                if result[0] is None:
                    my_retire_path_list['blank'].append(img_path)
                elif len(result) == 1:
                    if result[0].shape[0] / img.shape[0] > 0.7:
                        my_img_path_list.append(img_path)
                        my_imgs.append(img)
                    else:
                        my_retire_path_list['blank'].append(img_path)
                else:
                    my_retire_path_list['multi'].append(img_path)
            return my_img_path_list, my_retire_path_list, my_imgs

        def ocr_judge(img_path_list, imgs, results1, results2):
            def judge_category(text):
                if re.match(self.rep, text) is not None:
                    if len(text) < 7:
                        return '1'
                    elif len(text) < 17:
                        return '2'
                else:
                    if len(text) < 7:
                        return '2'
                    if len(text) < 17:
                        return '3'
                return '0.5'

            my_retire_path_list, my_imgs = [], []
            my_img_path_list = {'1': [], '2': [], '3': [], '0.5': []}
            for img_path, img, result1, result2 in zip(img_path_list, imgs, results1, results2):
                if not result1 == result2:
                    category = judge_category(result1)
                    my_img_path_list[category].append({'filename': img_path, 'text': result1})
                    my_imgs.append(img)
                else:
                    my_retire_path_list.append({'filename': img_path, 'text': result1})
            return my_img_path_list, my_retire_path_list, my_imgs

        image_path_list = sorted(img_glob(image_dir))
        load_batch_size = 256

        remove_dir, dataset_dir = self._prepare_output_dir(image_dir)

        move_func = {'move': self.rename_and_move, 'copy': self.rename_and_copy}

        dataset_num_list = {'1': 0, '2': 0, '3': 0, '0.5': 0}
        reload_model_counter = 1
        for i in range(0, len(image_path_list), load_batch_size):
            batch_image_path_list = image_path_list[i:i + load_batch_size]
            imgs = [image_preprocessing(cv2.imread(p)) for p in batch_image_path_list]
            load_image_size = len(batch_image_path_list)

            # written only
            blank_results = self.blank_recognizer.predict(imgs)
            batch_image_path_list, retire_path_list, imgs = blank_judge(batch_image_path_list, imgs, blank_results)
            move_func[move_type](retire_path_list, remove_dir['empty'])
            print('blank sorting remove num:', len(retire_path_list))
            if len(batch_image_path_list) == 0:
                continue

            # one line only
            detect_results, _ = self.detection_recognizer.predict(imgs)
            batch_image_path_list, retire_path_list, imgs = detection_judge(batch_image_path_list, imgs, detect_results)
            move_func[move_type](retire_path_list['multi'], remove_dir['detection'])
            move_func[move_type](retire_path_list['blank'], remove_dir['empty'])
            print('detect sorting remove num: multi_line {} blank {}'.format(len(retire_path_list['multi']),
                                                                             len(retire_path_list['blank'])))
            if len(batch_image_path_list) == 0:
                continue

            # different result only
            ocr_results = self.ocr_recognizer.predict(imgs)
            old_ocr_results = self.old_ocr_recognizer.predict(imgs)
            batch_image_path_list, retire_path_list, imgs = ocr_judge(batch_image_path_list, imgs, ocr_results, old_ocr_results)
            move_func[move_type](retire_path_list, remove_dir['ocr'])
            print('ocr sorting remove num:', len(retire_path_list))

            for data_type in batch_image_path_list:
                move_func[move_type](batch_image_path_list[data_type], dataset_dir[data_type])

            for t in batch_image_path_list:
                dataset_num_list[t] += len(batch_image_path_list[t])

            dataset_num = sum([dataset_num_list[t] for t in dataset_num_list])
            print(json.dumps(dataset_num_list, indent=1))
            print('{} / {} is elected'.format(dataset_num, i + load_image_size))
            print('{} / {} is finish'.format(i + load_image_size, len(image_path_list)))

            # if reload_model_counter > 100:
            #     print('refresh_model')
            #     self.refresh_model()
            #     reload_model_counter = 0
            reload_model_counter += 1


def main():
    # Parsing arguments
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('image_dir', type=str)
    parser.add_argument('read_data_type', type=str)
    parser.add_argument('--work_space_path', type=str, default=os.path.join(os.path.expanduser('~'), '.plantsorter'))
    parser.add_argument('--batch_size', type=int, default=16)
    parser.add_argument('--is_test', type=bool, default=False)
    parser.add_argument('--move_type', type=str, default='move')
    parser.add_argument('--detect_test', type=int, default=0)

    args = parser.parse_args()

    os.makedirs(args.work_space_path, exist_ok=True)

    # if args.detect_test == 1:
    # Sorter.detect_tester(args.image_dir, 'detect_test_output', args.work_space_path)

    sorter = Sorter(work_space_path=args.work_space_path, batch_size=args.batch_size,
                    read_model_type=args.read_data_type)

    if args.is_test:
        sorter.recognizer_tester(image_dir=args.image_dir)
    else:
        sorter.sorting_flow(image_dir=args.image_dir, move_type=args.move_type)


if __name__ == '__main__':
    main()
