from .blank_sorter import BlankRecognizer
from .detection_sorter import DetectionSorter
from .ocr_sorter import AllRecognizer