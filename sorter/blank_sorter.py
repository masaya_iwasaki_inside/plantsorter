import h5py, os
from keras.models import load_model
import cv2
import numpy as np


class BlankRecognizer(object):
    def __init__(self, modelfile, batch_size=16):
        self.modelfile = modelfile
        self.batch_size = batch_size
        self.__load()

    def __load(self):
        self.model = load_model(self.modelfile, compile=False)
        self.model._make_predict_function()
        with h5py.File(self.modelfile, 'r') as h5file:
            self.classes = [s.decode('utf8') for s in np.asarray(
                          h5file[os.path.join('ai_inside', 'classes')].value).flatten()]

    def __norm(self, images):
        imgs = np.array([]).reshape(0, self.model.input_shape[1], self.model.input_shape[2], 1)
        for img in images:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.resize(img, (self.model.input_shape[2], self.model.input_shape[1]))
            img = np.asarray(img, dtype=np.uint8)
            img = np.invert(img)
            img = np.expand_dims(img, axis=3)
            img = np.expand_dims(img, axis=0)
            imgs = np.append(imgs, img, axis=0)
        return imgs

    def predict(self, imgs):
        imgs = self.__norm(imgs)
        out = self.model.predict(imgs, batch_size=self.batch_size)
        labels = np.argmax(out, axis=-1)
        return [self.classes[i] for i in labels]

