import os
import math
import h5py
import cv2
import numpy as np
from keras.models import load_model


class AllRecognizer(object):
    def __init__(self, model_file, scaling=True, batch_size=16):
        model = load_model(model_file, compile=False)
        model._make_predict_function()
        self.model = model
        with h5py.File(model_file, 'r') as h5file:
            classes = [s.decode('utf8') for s in np.asarray(
                          h5file[os.path.join('ai_inside', 'classes')].value).flatten()]
        self.classes = classes
        self.constant_input_shape = (None, 64, None, 3)
        self.input_ratio = 16
        self.max_width = 2048
        self.batch_size = batch_size
        self.scaling = scaling

    def __norm(self, imgs):
        height = self.constant_input_shape[1]
        ch = self.constant_input_shape[3]
        max_aspect_ratio = max([i.shape[1] / i.shape[0] for i in imgs])
        width = min(self.max_width, int(math.ceil(max_aspect_ratio * height / self.input_ratio) * self.input_ratio))

        batch_shape = (len(imgs), height, width, ch)
        input_data = np.zeros(batch_shape, dtype=np.float64)
        for index, box_image in enumerate(imgs):
            box_image = self._resize_image_keeping_aspect_ratio(box_image,
                                                                constant_input_shape=(None, height, width, ch))
            input_data[index, :, :, :] = box_image
        return input_data / 255. if self.scaling else input_data

    def predict(self, imgs):
        imgs = self.__norm(imgs)
        texts = []
        out = self.model.predict(imgs, batch_size=self.batch_size)
        labels = self._greedy_decode(out)
        for index, label in enumerate(labels):
            text = ''.join([self.classes[t] for t in label])
            texts.append(text)
        return texts

    @classmethod
    def _greedy_decode(cls, softmax):
        labels = []
        outs_index_list = np.argmax(softmax, axis=-1)
        max_index = softmax.shape[-1] - 1
        for out_index_list in outs_index_list:
            label = []
            pre_index = max_index
            for index in out_index_list:
                if pre_index != index and index != max_index:
                    label.append(index)
                pre_index = index
            labels.append(label)
        return labels

    def _resize_image_keeping_aspect_ratio(self, img, remove_top_bottom_as_well=True, constant_input_shape=None):
        # FIXME:batch support
        if img.shape[0] * img.shape[1] <= 0:
            return img

        # remove the redundant space on both of right and left side
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(np.invert(thresh))
        if remove_top_bottom_as_well:
            if w > 0 and h > 0:
                img = img[y: y + h, x: x + w, :]
        else:
            if w > 0:
                img = img[:, x: x + w, :]

        # change the height to the target one
        if constant_input_shape is None:
            _, input_w, input_h, input_ch = self.model.input_shape
        else:
            _, input_h, input_w, input_ch = constant_input_shape
        img = cv2.resize(img, (max(1, input_h * img.shape[1] // img.shape[0]), input_h),
                         interpolation=cv2.INTER_CUBIC)

        # make the image shrink the width if it is still long
        if input_w < img.shape[1]:
            img = cv2.resize(img, (input_w, input_h),
                                   interpolation=cv2.INTER_CUBIC)

        # otherwise, add blank space to the right so that the size of the image can be the given one
        elif input_w >= img.shape[1]:
            img = self._set_margin(img, margins=(0, input_w - img.shape[1], 0, 0))

        return img

    @classmethod
    def _set_margin(cls, img, margins=(10, 10, 10, 10), background_color=255):
        canvas = np.zeros((img.shape[0] + margins[0] + margins[2],
                           img.shape[1] + margins[1] + margins[3], img.shape[-1]), np.uint8) + background_color
        y1, y2 = margins[0], margins[0] + img.shape[0]
        x1, x2 = margins[3], margins[3] + img.shape[1]
        canvas[y1: y2, x1: x2, :] = img
        return canvas
