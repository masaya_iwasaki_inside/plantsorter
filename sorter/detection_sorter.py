import numpy as np
from functools import cmp_to_key
from skimage.morphology import label as bwlabel
import tensorflow as tf
from keras.models import load_model
import keras.backend as K


def detection_softmax(x):
    ndim = K.ndim(x)
    if ndim == 2:
        return K.softmax(x)
    elif ndim == 3:
        e = K.exp(x - K.max(x, axis=-1, keepdims=True))
        s = K.sum(e, axis=-1, keepdims=True)
        return e / s
    elif ndim == 4:
        e = K.exp(x - K.max(x, axis=1, keepdims=True))
        s = K.sum(e, axis=1, keepdims=True)
        return e / s
    else:
        raise Exception('Cannot apply softmax to a tensor that is not 2D or 3D. ' +
                        'Here, ndim=' + str(ndim))


class MultilinesBaseRecognizer(object):
    """
    Provide bunch of methods for multi lines support
    """

    def detect_lines(self, model, image, likelihood=0.3, min_size=20, full_width=True, detection_with_cpu=False):
        image_h, image_w = image.shape[0:2]
        if image_h * image_w <= 0:
            return [image], None

        th_tensor = self._prepare_input(image, 'multiple')
        if th_tensor.mean() < 0:
            th_tensor = -th_tensor

        res_map = self._decode_image(model, th_tensor, image.shape[:2], detection_with_cpu)
        bbox_lut = self._from_res_map_to_bbox(res_map)
        boxes = []
        for index, (top, bot, left, right) in enumerate(bbox_lut['bounding_box']):
            if likelihood < bbox_lut['proba'][index] and min_size <= bot - top and min_size <= right - left:
                boxes.append((top, bot, left, right))

        boxes = self._merge_boxes(boxes, min_size)

        if len(boxes) == 0:
            return [None], None

        boxes = sorted(boxes, key=cmp_to_key(self._compare_boxes))

        '''
        # make it a little bit larger
        for index, (top, bot, left, right) in enumerate(boxes):
            left = 0 if full_width else int(max(0, right - 1.3 * (right - left)))
            right = image_w if full_width else int(min(image_w, left + 1.3 * (right - left)))
            boxes[index] = (top, bot, left, right)
        '''

        rtn_images = []
        for (top, bot, left, right) in boxes:
            rtn_images.append(image[top:bot, left:right, :])

        return rtn_images, boxes

    def _merge_boxes(self, boxes, min_size):
        for i in range(len(boxes)):
            for j in range(i + 1, len(boxes)):
                top1, bot1, left1, right1 = boxes[i]
                top2, bot2, left2, right2 = boxes[j]
                h_max = bot1 - top1
                h_min = bot2 - top2

                dup_h = min(bot1, bot2) - max(top1, top2)
                dup_w = min(right1, right2) - max(left1, left2)
                # see if these two boxes need to be merged
                if h_max < h_min:
                    h_max, h_min = h_min, h_max
                if h_max * 0.8 < dup_h or \
                        (h_min * 0.9 < dup_h and dup_w > 0):
                    boxes[i] = (min(top1, top2), max(bot1, bot2), min(left1, left2), max(right1, right2))
                    del boxes[j]
                    return self._merge_boxes(boxes, min_size)
        return boxes

    @classmethod
    def _relax_wrt_border(cls, raw_bbox, height, width, border_perc=.32):
        # load bbox
        top, bot, left, right = raw_bbox
        # compute box width and height
        box_w, box_h = right - left + 1, bot - top + 1
        # compute border width
        d = int(np.ceil((box_h * border_perc + 0.5) * 0.5))
        # relax according to border info
        left, right = max(0, left - d), min(right + d, width)
        top, bot = max(0, top - d), min(bot + d, height)
        return [top, bot, left, right]

    def _from_res_map_to_bbox(self, res_map, th_size=8, th_prob=0.25, border_perc=.32):
        height, width = res_map.shape[:2]
        labels = res_map.argmax(axis=-1)
        text = labels == 2
        bwtext, nb_regs = bwlabel(text, return_num=True)
        lut = {'bounding_box': [], 'proba': []}
        for reg_id in range(1, nb_regs + 1):
            row_idx, col_idx = np.nonzero(bwtext == reg_id)
            # get four corners
            left, right = col_idx.min(), col_idx.max() + 1
            top, bot = row_idx.min(), row_idx.max() + 1
            # relax w.r.t. border
            bbox = self._relax_wrt_border([top, bot, left, right], height, width, border_perc)
            by0, by1, bx0, bx1 = bbox
            bh, bw = by1 - by0 + 1, bx1 - bx0 + 1
            # estimate text proba
            proba = np.median(res_map[top:bot, left:right, 2])
            if (proba >= th_prob) and (min(bh, bw) >= th_size):
                lut['bounding_box'].append([by0, by1, bx0, bx1])
                lut['proba'].append(float(proba))
        return lut

    @classmethod
    def _load_detection_model(cls, model_file_path, detection_with_cpu):
        if detection_with_cpu:
            with tf.device('/cpu:0'):
                return load_model(
                    model_file_path,
                    compile=False,
                    custom_objects={'custom_softmax': detection_softmax}
                )
        else:
            return load_model(
                model_file_path,
                compile=False,
                custom_objects={'custom_softmax': detection_softmax}
            )

    @classmethod
    def _prepare_input(cls, image_array, res_type='multiple'):
        """
        Prepare input image array to tensor
        """
        if res_type == 'single':
            multiple_of_x = 8
        else:  # multiple
            multiple_of_x = 96

        # determine padding patterns
        h, w = image_array.shape[:2]
        pad_h = (h // multiple_of_x * multiple_of_x - h) % multiple_of_x
        pad_w = (w // multiple_of_x * multiple_of_x - w) % multiple_of_x
        # pad image to make sure the new dimension is a multiple of ${multiple_of_X}
        image_pad = np.pad(image_array, ([0, pad_h], [0, pad_w], [0, 0]),
                           mode='symmetric')
        # convert image uint8 array to theano float32 tensor
        th_tensor = np.rollaxis(image_pad.astype(np.float32), 2, 0)
        th_tensor = np.expand_dims(th_tensor, 0) / 255. - 0.5
        return th_tensor

    @classmethod
    def _decode_image(cls, model, th_tensor, output_shape, detection_with_cpu):
        """
        Decode a PPT image using an existing model
        """
        h, w = output_shape
        if detection_with_cpu:
            with tf.device('/cpu:0'):
                res_map = model.predict(th_tensor)
        else:
            res_map = model.predict(th_tensor)
        res_map = np.rollaxis(res_map[0], 0, 3)
        res_map = res_map[:h, :w]
        return res_map

    @classmethod
    def _compare_boxes(cls, x, y):
        t1, b1, l1, r1 = x
        t2, b2, l2, r2 = y
        t = max(t1, t2)
        b = min(b1, b2)
        if t <= b and ((r2 - l2) * 3 // 5 + l2 < l1):
            return 1
        elif t <= b and ((r1 - l1) * 3 // 5 + l1 < l2):
            return -1
        else:
            return x[0] - y[0]


class DetectionSorter(MultilinesBaseRecognizer):
    def __init__(self, model_path, detection_with_cpu=False):
        self.model_path = model_path
        self.detection_with_cpu = detection_with_cpu
        self.detection_model = self._load_detection_model(self.model_path, self.detection_with_cpu)

    def predict(self, imgs):
        output_images = []
        output_boxes = []
        for img in imgs:
            box_images, boxes = self.detect_lines(self.detection_model, img,
                                           detection_with_cpu=self.detection_with_cpu)
            output_images.append(box_images)
            output_boxes.append(boxes)
        return output_images, output_boxes




