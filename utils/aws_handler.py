import boto3, os, shutil
from urllib.parse import urlsplit
import re


class aws_handler(object):
    # FIXME: you should use OS.Environ['hoge']
    s3_client = boto3.session.Session(
            aws_access_key_id='AKIAIGVGH4DJTGP736WA',
            aws_secret_access_key='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',
            region_name='ap-northeast-1').client('s3')

    @classmethod
    def download_file(cls, src_s3_path, src_local_path):
        if os.path.isdir(src_local_path):
            src_local_path = os.path.join(src_local_path, os.path.basename(src_s3_path))
        if os.path.exists(src_local_path):
            return src_local_path
        os.makedirs(os.path.dirname(src_local_path), exist_ok=True)
        s3_uri = urlsplit(src_s3_path)
        print('download', src_s3_path, 'to', src_local_path)
        cls.s3_client.download_file(s3_uri.netloc, s3_uri.path[1:], src_local_path)
        return src_local_path

    @classmethod
    def download_zip(cls, stc_datasets_s3_path, dst_local_dir_path, remove_zip=False):
        s3_uri = urlsplit(stc_datasets_s3_path)
        aws_all_path = s3_uri.netloc+s3_uri.path.replace('/', '_').replace('s3://', '')
        os.makedirs(dst_local_dir_path, exist_ok=True)
        local_zip_path = os.path.join(dst_local_dir_path, os.path.basename(aws_all_path))
        extract_dir_path = os.path.join(dst_local_dir_path,
                                        os.path.splitext(os.path.basename(stc_datasets_s3_path))[0])
        if not os.path.exists(extract_dir_path):
            cls.download_file(stc_datasets_s3_path, local_zip_path)
            os.makedirs(extract_dir_path, exist_ok=True)
            os.system('unzip {0} -d {1}'.format(local_zip_path, os.path.dirname(extract_dir_path)))
            if remove_zip:
                os.system('rm {0}'.format(local_zip_path))
        datasets_dir_path = extract_dir_path
        return datasets_dir_path

    @classmethod
    def ls(cls, src_s3_path):
        s3_uri = urlsplit(src_s3_path)
        response = cls.s3_client.list_objects(Bucket=s3_uri.netloc, Prefix=s3_uri.path[1:])
        keys = []
        if 'Contents' in response:
            keys = [os.path.join('s3://'+s3_uri.netloc, content['Key']) for content in response['Contents']]
        return keys

    @classmethod
    def download_real_data(cls, download_datasets_s3_path, local_dir_path):
        def zip_data_download_by_os_system(download_datasets_s3_path, local_dir_path):
            aws_all_path = download_datasets_s3_path
            os.makedirs(local_dir_path, exist_ok=True)
            local_zip_path = os.path.join(local_dir_path, os.path.basename(aws_all_path))
            extract_dir_path = os.path.join(os.path.dirname(local_zip_path),
                                            os.path.splitext(os.path.basename(local_zip_path))[0])
            if not os.path.exists(local_zip_path):
                cls.download_file(download_datasets_s3_path, local_zip_path)
                os.makedirs(extract_dir_path)
                os.system('unzip {0} -d {1}'.format(local_zip_path, os.path.dirname(extract_dir_path)))
            return extract_dir_path

        key_list = cls.ls(download_datasets_s3_path)
        for key in key_list:
            if not re.match(os.path.join(download_datasets_s3_path, '.*.zip'), key):
                continue
            dataset_path = zip_data_download_by_os_system(key, local_dir_path)
            mac_dir_path = os.path.join(dataset_path, '../__MACOSX')
            if os.path.exists(mac_dir_path):
                shutil.rmtree(mac_dir_path, ignore_errors=True)
        return local_dir_path
