import os
import glob
import re
import cv2
import numpy as np


def img_glob(path):
    ext = '.*\.(jpg|jpeg|png|bmp)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def json_glob(path):
    ext = '.*\.(json)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def image_preprocessing(img, remove_top_bottom_as_well=True):
    # FIXME:batch support
    if img.shape[0] * img.shape[1] <= 0:
        return img

    # remove the redundant space on both of right and left side
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    x, y, w, h = cv2.boundingRect(np.invert(thresh))
    if remove_top_bottom_as_well:
        if w > 0 and h > 0:
            img = img[y: y + h, x: x + w, :]
    else:
        if w > 0:
            img = img[:, x: x + w, :]
    return img
